# Generated by Django 5.0.2 on 2024-05-09 12:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DjangoEcommerceApp', '0002_alter_customeruser_profile_pic_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customeruser',
            name='profile_pic',
            field=models.FileField(default='', upload_to=''),
        ),
        migrations.AlterField(
            model_name='merchantuser',
            name='profile_pic',
            field=models.FileField(default='', upload_to=''),
        ),
    ]
